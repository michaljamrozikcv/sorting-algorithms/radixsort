package com.example.radixSort;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] array = RadixSort.createRandomArray(30, 1, 200);
        System.out.println("Array before sorting:");
        System.out.println(Arrays.toString(array));
        System.out.println("Sorted array:");
        RadixSort.radixSort(array);
        System.out.println(Arrays.toString(array));
    }
}
