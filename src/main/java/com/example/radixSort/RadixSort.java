package com.example.radixSort;

import java.util.Arrays;
import java.util.Random;

class RadixSort {
    /**
     * złożoność obliczeniowa: O(n^2)
     * złożoność pamięciowa: Omega(n)
     */
    static void radixSort(int[] array) {
        int divider = 1;
        int[][] buckets = new int[array.length][10];
        //get max value in array
        int max = Arrays.stream(array)
                .max().getAsInt();
        //count quantity of digits in max value
        int quantityOfDigits = 1;
        while (max / 10 != 0) {
            max = max / 10;
            quantityOfDigits++;
        }
        for (int i = 0; i <= quantityOfDigits; i++) {
            putIntoBuckets(array, divider, buckets);
            reorderOriginalArray(array, buckets);
            divider *= 10;
        }
    }

    private static void putIntoBuckets(int[] array, int divider, int[][] buckets) {
        for (int i = 0; i < array.length; i++) {
            int tempIndex = 0;
            while (buckets[tempIndex][(array[i] / divider) % 10] != 0) {
                tempIndex++;
            }
            buckets[tempIndex][(array[i] / divider) % 10] = array[i];
        }
    }

    private static void reorderOriginalArray(int[] array, int[][] buckets) {
        int tempIndex = 0;
        for (int j = 0; j < 10; j++) {
            for (int i = 0; i < buckets.length; i++) {
                if (buckets[i][j] != 0) {
                    array[tempIndex] = buckets[i][j];
                    //reset buckets
                    buckets[i][j] = 0;
                    tempIndex++;
                }
            }
        }
    }

    /**
     * Return array of random integers
     *
     * @param size   the number of values to generate
     * @param origin the origin (inclusive) of each random value
     * @param bound  the bound (exclusive) of each random value
     */
    static int[] createRandomArray(int size, int origin, int bound) {
        int[] array = new int[size];
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            array[i] = random.ints(1, origin, bound).findFirst().getAsInt();
        }
        return array;
    }
}
